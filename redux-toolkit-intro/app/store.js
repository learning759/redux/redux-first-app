const {
    configureStore
} = require('@reduxjs/toolkit');

const {createLogger} = require('redux-logger')

const logger = createLogger();

const {reducer: cakeReducer} = require('../features/cake/cakeSlice');
const {reducer: icecreamReducer} = require('../features/icecream/icecreamSlice');
const {reducer: userReducer} = require('../features/user/userSlice');

const store = configureStore({
    reducer: {
        cake: cakeReducer,
        icecream: icecreamReducer,
        user: userReducer
    }
});

module.exports = {
    store
};