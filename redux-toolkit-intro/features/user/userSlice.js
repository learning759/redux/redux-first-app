const {
    createSlice,
    createAsyncThunk
} = require('@reduxjs/toolkit');

const axios = require('axios');

const initialState = {
    loading: false,
    users: [],
    error: ''
};

// async action using thunk --- function that accepts 2 arguments (actionType, callbackFunction which contains async logic and returns promise), Generates pending, fullfilled and rejected action types
const fetchUsers = createAsyncThunk('user/fetchUsers', ()=>{
    return axios.get('https://jsonplaceholder.typicode.com/users').then(res=> {
        const users = res.data.map(user=> user.id)
        return users;
    });
})

const userSlice = createSlice({
    name: 'user',
    initialState,
    extraReducers: (builder)=>{
        builder.addCase(fetchUsers.pending, state=>{
            state.loading = true;
        })
        builder.addCase(fetchUsers.fulfilled, (state, {payload})=>{
            state.loading = false;
            state.users = payload;
        })
        builder.addCase(fetchUsers.rejected, (state, action)=>{
            state.loading = false;
             state.error = action.error.message;
        })
    }
});

module.exports = {
    reducer: userSlice.reducer,
    userActions: userSlice.actions,
    fetchUsers
};