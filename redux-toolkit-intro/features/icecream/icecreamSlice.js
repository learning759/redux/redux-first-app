const {
    createSlice
} = require('@reduxjs/toolkit');

const initialState = {
    numOfIcecream: 20
};

const icecreamSlice = createSlice({
    name: 'icecream',
    initialState,
    reducers: {
        ordered: (state, {payload=1})=>{
            state.numOfIcecream -= payload;
        },
        restocked: (state, {payload=1})=>{
            state.numOfIcecream += payload;
        }
    },
    extraReducers: {
        ['cake/ordered']: (state)=>{
            state.numOfIcecream--;
        }
    }
});

// extrareducer can be written in two ways, one way is given above. better way is given in cakeSlice module


module.exports = {
    reducer: icecreamSlice.reducer,
    icecreamActions: icecreamSlice.actions
};