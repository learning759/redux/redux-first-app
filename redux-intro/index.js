const {
  legacy_createStore: createStore,
  bindActionCreators,
  combineReducers,
  applyMiddleware
} = require('redux');

const {
  createLogger
}= require('redux-logger');
const logger = createLogger();

const CAKE_ORDERED = 'CAKE_ORDERED';
const RESTOCK_CAKE = 'RESTOCK_CAKE';
const ICE_CREAM_ORDERED = 'ICE_CREAM_ORDERED';
const RESTOCK_ICE_CREAM = 'RESTOCK_ICE_CREAM';

// action creator -- return object that contains 'type'
function orderCake (num =1){
  return {
    type: CAKE_ORDERED,
    payload: num
  };
};

function restockCake (cakeNum=1){
  return {
    type: RESTOCK_CAKE,
    payload: cakeNum
  };
};

function orderIceCream(num = 1){
  return {
    type: ICE_CREAM_ORDERED,
    quantity: num
  };
};

function restockIceCream (cakeNum=1){
  return {
    type: RESTOCK_ICE_CREAM,
    payload: cakeNum
  };
};


// const initialState = {
//   numOfCakes: 10,
//   numOfIceCreams: 20
// };

const intialCakeState = {
  numOfCakes: 10
};

const intialIceCreamState = {
  numOfIceCream: 20 
};

// (prevState, action) => newState -- reducer
// 0

// const store = createStore(reducer);
// console.log('intialState', store.getState());

// const unsubscribe = store.subscribe(()=> console.log("updated state", store.getState()));
// // store.dispatch(orderCake());
// // store.dispatch(orderCake());
// // store.dispatch(orderCake());
// // store.dispatch(restockCake(3));

// const actions = bindActionCreators({orderCake, restockCake}, store.dispatch);
// actions.orderCake()
// actions.orderCake()
// actions.orderCake()
// actions.restockCake(3)
// unsubscribe()

// multiple reducer case
const cakeReducer = (state= intialCakeState, action)=>{
  switch(action.type){
    case CAKE_ORDERED: return {
      ...state,
      numOfCakes: state.numOfCakes -1
    };
    case RESTOCK_CAKE: return {
      ...state,
      numOfCakes: state.numOfCakes + action.payload
    };
    default: return state;
  };
};

const iceCreamReducer = (state= intialIceCreamState, action)=>{
  switch(action.type){
    case ICE_CREAM_ORDERED: return {
      ...state,
      numOfIceCream: state.numOfIceCream -1
    };
    case RESTOCK_ICE_CREAM: return {
      ...state,
      numOfIceCream: state.numOfIceCream + action.payload
    };
    default: return state;
  };
};

const rootReducer = combineReducers({
  cake: cakeReducer,
  iceCream: iceCreamReducer
});

const store = createStore(rootReducer, applyMiddleware(logger));
console.log('intialState', store.getState());

const unsubscribe = store.subscribe(()=> {});

const actions = bindActionCreators({
  orderCake,
  restockCake,
  orderIceCream,
  restockIceCream
}, store.dispatch);

actions.orderCake()
actions.orderCake()
actions.orderCake()
actions.restockCake(3)
actions.orderIceCream()
actions.orderIceCream()
actions.orderIceCream()
actions.restockIceCream(3)
unsubscribe()
