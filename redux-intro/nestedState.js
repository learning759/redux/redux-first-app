const {
    legacy_createStore: createStore
} = require('redux');

const {produce} = require('immer');

const intialState = {
    name: 'Nevin Edwin',
    address: {
        street: 'Valiathura',
        city: 'Trivandrum',
        state: 'Kerala'
    }
}


const STREET_UPDATED = 'STREET_UPDATED';
const STREET_UPDATED_IMMER = 'STREET_UPDATED_IMMER';

const streetUpdate = (payload)=>{
    return {
        type: STREET_UPDATED,
        payload
    };
};

const streetUpdateImmer = (payload)=>{
    return {
        type: STREET_UPDATED_IMMER,
        payload
    };
};

const reducer = (state=intialState, action)=>{
    switch (action.type){
        case STREET_UPDATED: return {
            ...state,
            address: {
                ...state.address,
                street: action.payload
            }
        };
        case STREET_UPDATED_IMMER: return produce(state, (draft)=>{
            draft.address.street = action.payload
        })
        default: return state;
    };
};

const store = createStore(reducer);
console.log('intialState', store.getState());

const unsubscribe = store.subscribe(()=> console.log('updatedState', store.getState()));

store.dispatch(streetUpdate('eastFort'));
store.dispatch(streetUpdateImmer('westFort'));

unsubscribe()