const {
    legacy_createStore: createStore,
    applyMiddleware
} = require('redux');

const thunkMiddleWare = require('redux-thunk').default

const axios = require('axios')
const initialState = {
    loading: false,
    users: [],
    error: ""
};

const FETCH_USERS_REQUESTED = 'FETCH_USERS_REQUESTED';
const FETCH_USERS_SUCCEEDED = 'FETCH_USERS_SUCCEEDED';
const FETCH_USERS_FAILED = 'FETCH_USERS_FAILED';

const fetchUsersRequested = ()=>{
    return {
        type: FETCH_USERS_REQUESTED
    };
};

const fetchUsersSucceeded = (users)=>{
    return {
        type: FETCH_USERS_SUCCEEDED,
        payload: users
    };
};

const fetchUsersFailure = (error)=>{
    return {
        type: FETCH_USERS_FAILED,
        payload: error
    };
};

const reducer = (state=initialState, action)=>{
    switch (action.type){
        case FETCH_USERS_REQUESTED: return {
            ...state,
            loading: true
        };
        case FETCH_USERS_SUCCEEDED: return {
            ...state,
            loading: false,
            users: action.payload
        };
        case FETCH_USERS_FAILED: return {
            ...state,
            loading: false,
            error: action.payload
        };
        default: return state;
    };
};


// async action creator
const fetchUsers = ()=>{
    return (dispatch)=>{
        dispatch(fetchUsersRequested());
        axios.get("https://jsonplaceholder.typicode.com/users").then((res)=>{
            const users = res.data.map(user=> user.id);
            dispatch(fetchUsersSucceeded(users));
        }).catch(err=> {
            dispatch(fetchUsersFailure(err.message));
        });
    };
};

const store = createStore(reducer, applyMiddleware(thunkMiddleWare));

store.subscribe(()=>{console.log(store.getState())});
store.dispatch(fetchUsers());
// unsubscribe();
