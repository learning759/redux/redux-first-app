import React, { useState } from 'react';
import {useDispatch, useSelector} from 'react-redux';
import { cakeActions } from './cakeSlice';

const CakeView = () => {
  const [restockVal, setRestockVal] = useState(1);
  const numOfCakes = useSelector((state)=>state.cake.numOfCakes);
  const dispatch = useDispatch();
  return (
    <div>
        <h2>Number of Cakes - {numOfCakes}</h2>
        <button onClick={()=> dispatch(cakeActions.ordered())}>Order Cake</button>
        <input type="number" value={restockVal} onChange={(e)=> setRestockVal(+(e.target.value))}/>
        <button onClick={()=> dispatch(cakeActions.restocked(restockVal))}>Restock Cake</button>
    </div>
  )
}

export default CakeView;