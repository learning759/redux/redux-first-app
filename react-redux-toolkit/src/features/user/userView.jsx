import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { fetchUsers } from './userSlice';

const UserView = () => {
  const user = useSelector(state=> state.user);
  const dispatch = useDispatch();

  useEffect(()=>{
    dispatch(fetchUsers());
  }, [])

  return (
    <div>
        <h2>List of Users</h2>
        {user.loading && <div>Loading</div>}
        {!user.loading && user.error && <div>{user.error}</div>}
        {!user.loading && user.users.length && user.users.map((eachUser)=>{
          return(
            <li key={eachUser}>{eachUser}</li>
          )
        })
        }
    </div>
  )
}

export default UserView;