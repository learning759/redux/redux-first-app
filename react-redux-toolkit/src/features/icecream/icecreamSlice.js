import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    numOfIcecream: 20
};

const icecreamSlice = createSlice({
    name: 'icecream',
    initialState,
    reducers: {
        ordered: (state)=>{
            state.numOfIcecream--;
        },
        restocked: (state, action)=>{
            state.numOfIcecream += action.payload;
        }
    },
    extraReducers: {
        ['cake/ordered']: (state)=>{
            state.numOfIcecream--;
        }
    }
});

// extrareducer can be written in two ways, one way is given above. better way is given in cakeSlice module


export default icecreamSlice.reducer;
export const icecreamActions = icecreamSlice.actions;