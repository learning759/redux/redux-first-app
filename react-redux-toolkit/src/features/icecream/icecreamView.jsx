import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import {icecreamActions } from './icecreamSlice'

const IcecreamView = () => {
  const [restockVal, setRestockVal] = useState(1);
  const numOfIcecreams = useSelector((state)=> state.icecream.numOfIcecream);
  const dispatch = useDispatch();

  return (
    <div>
        <h2>Number of icecreams - {numOfIcecreams}</h2>
        <button onClick={()=> dispatch(icecreamActions.ordered())}>Order icecream</button>
        <input type="number" value={restockVal} onChange={(e)=>setRestockVal(+(e.target.value))}/>
        <button onClick={()=> dispatch(icecreamActions.restocked(restockVal))}>Restock icecreams</button>
    </div>
  )
}

export default IcecreamView;