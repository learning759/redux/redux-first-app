# redux-first-app

## Name
Redux ToolKit and Redux Learning

## Installation
node and npm in neccesary
1. npm init --y
2. npm i redux redux-thunk immer axios

## Usage
Learning Redux and Redux toolkit

## Support
1. https://www.youtube.com/watch?v=fG0Y7JbYSfY&list=PLC3y8-rFHvwiaOAuTtVXittwybYIorRB3&index=18

## Roadmap
1. See Redux-into folder - it contains how redux works and redux principles

## Authors and acknowledgment
Nevin Edwin

## License
For open source projects, say how it is licensed.

## Project status
developing
